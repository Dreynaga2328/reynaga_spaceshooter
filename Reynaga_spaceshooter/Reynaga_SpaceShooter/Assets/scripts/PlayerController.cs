﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{

}

public class PlayerController : MonoBehaviour

{
    
    private Rigidbody rb;
    private AudioSource audioSource;
    public float speed;
    public float tilt;
    public Boundary boundary;
    public float xMin, xMax, zMin, zMax;



    public GameObject shot;
    public Transform[] shotSpawns;
    public float fireRate;

    private float nextFire;


    void Update()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            for (int i = 0; i < shotSpawns.Length; i++)
            {

                Instantiate(shot, shotSpawns[i].position, shotSpawns[i].rotation); // as game object
            }
            audioSource.Play();
        }
        
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }


    void FixedUpdate ()
   {
        float moveHorizontal = Input.GetAxis ("Horizontal");
        float moveVertical = Input.GetAxis ("Vertical");

        Vector3 movement= new Vector3 (moveHorizontal, 0.0f, moveVertical);
        rb.velocity = movement * speed;

        rb.position = new Vector3
        (
           Mathf.Clamp (rb.position.x, xMin, xMax),
           0.0f,
           Mathf.Clamp (rb.position.z, zMin, zMax)
        );

        rb.rotation = Quaternion.Euler (0.0f, 0.0f, rb.velocity.x * -tilt);
       
    }

   

}