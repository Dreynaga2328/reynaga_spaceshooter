﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hitByBolt : MonoBehaviour
{
    private GameController gameController;

    bool hasCalled = false;
    private void OnTriggerEnter(Collider other)
    {
        if (hasCalled)
            return;

        print("SOME COLLISION!");
        if (other.CompareTag("playerBolt"))
        {
            print("HIT!");

            Destroy(gameObject);
            hasCalled = true;
            gameController.addStar();
        }
    }

    // Start is called before the first frame update
    

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");
        if (gameControllerObject != null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if (gameController == null)
        {
            Debug.Log("Cannot find 'GameController' script");
        }
    }
    void Update()
    {
        
    }
}
