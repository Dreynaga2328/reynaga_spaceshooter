﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public GameObject[] hazards;
    public Vector3 spawnValues;
    public int hazardCount;
    public float spawnWait;
    public float startWait;
    public float waveWait;

    public Text scoreText;
    public Text restartText;
    public Text gameOverText;
    public Text timerText;
    public Text winText;
    public Text highScoreText;



    float timer;


    private bool gameover;
    private bool restart;
    private int score;
    private int stars;

    public bool makeEnemies;

    int highScore;
   

    void Start()
    {

       
        gameover = false;
        restart = false;
        restartText.text = "";
        gameOverText.text = "";
        score = 0;
        UpdateScore();
        if (makeEnemies)
        StartCoroutine(SpawnWaves());
        SetTimerText();
        highScore = PlayerPrefs.GetInt("highScore", 0);
        highScoreText.text = "highScore" + highScore;


        print("high score" + highScore);

    }

    void Update()
    {
        
        timer += Time.deltaTime;
         SetTimerText();
        if (timer >= 30f) // sets the timer
        {
            print("Game Over");


        }

        if (restart)
        {
            if (Input.GetKeyDown (KeyCode.R))
            {
                Application.LoadLevel (Application.loadedLevel);
            }
    
        }
    } 


    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        while (true)
        {
            for (int i = 0; i < hazardCount; i++)
            {
                if (!makeEnemies)
                    break;
                GameObject hazard = hazards[Random.Range(0, hazards.Length)];
                Vector3 spawnPosition = new Vector3(Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
                Quaternion spawnRotation = Quaternion.identity;
                Instantiate(hazard, spawnPosition, spawnRotation);
                yield return new WaitForSeconds(spawnWait);
            }
            yield return new WaitForSeconds(waveWait);

            if (gameover)
            {
                restartText.text = "Press 'R' for Restart";
                restart = true;
                break;
            }
        }
    }

    public void AddScore(int newScoreValue)
    {

        score += newScoreValue;
        if (score > highScore)
        {
            highScore = score;
            highScoreText.text = "highScore" + highScore;
            PlayerPrefs.SetInt ("highScore", highScore); //saves scores
            

            print("high score" + highScore);
        }
        UpdateScore();

    }

  
    void UpdateScore()
    {
        scoreText.text = "Score: " + score;

    }
    
    public void addStar()
    {
        stars += 1;
        print("stars:" + stars);
        AddScore(50);

        if (stars >= 11)
        {
            winText.text = "YOU WIN";
            makeEnemies = false;
           
        }
    }
    public void GameOver ()
    {
        gameOverText.text = "Game Over!";
        gameover = true;
    }

    void SetTimerText() // timer added 
    {
        timerText.text = "Timer" + timer.ToString();

  
    }
}