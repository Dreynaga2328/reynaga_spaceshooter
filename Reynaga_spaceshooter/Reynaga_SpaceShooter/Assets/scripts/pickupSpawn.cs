﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickupSpawn : MonoBehaviour
{
    public GameObject[] pickupPrefabs;
    GameObject pickupSpawned;
    float waitToSpawn = 0f;

    void Start()
    {
        SpawnPickup();
    }

    void Update()
    {
      if (pickupSpawned == null)
        {
            if (waitToSpawn <= 0f)
                SpawnPickup();
            else
                waitToSpawn -= Time.deltaTime;
        }
    }

    void SpawnPickup()
    {
        GameObject pickupToSpawn = pickupPrefabs[(int)(Random.value * pickupPrefabs.Length)];
        pickupSpawned = Instantiate(pickupToSpawn, transform.position, pickupToSpawn.transform.rotation) as GameObject;
        waitToSpawn = Random.Range(20f, 40f);
    }
}
